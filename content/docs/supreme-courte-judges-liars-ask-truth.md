+++
date = "2019-01-26T06:54:02+00:00"
text_area = ""
the_text_heading = ""

+++
# सुप्रीम कोर्ट के भ्रष्ट जज ; झूठो ने झूठो से कहा है "सच बोलो"

सुप्रीम कोर्ट के भ्रष्ट जजों की आपसी थुक्का फजीहत ; झूठो ने झूठो से कहा है कि - "सच बोलो" !!!  
.   
भारत के राजनेता दुसरे नंबर का सबसे भ्रष्ट वर्ग है। पहला नंबर जजों का है। शायद इन लोगो में घूस के पैसे में बटवारें का कोई झगड़ा पड़ है। इसीलिए बाहर निकलकर एक दुसरे से हिसाब चुकता कर रहे है। बहरहाल , इस एपिसोड से सबसे ज्यादा धक्का न्यायमूर्ति पूजको को पहुंचा है। वे अब तक यह तय नहीं कर पा रहे है, कि अब जबकि "कुछ" मीलार्ड ने खुद ही आकर कह दिया है कि सुप्रीम कोर्ट भ्रष्ट है , तो हम भारत की न्यायपालिका एवं जजों को भ्रष्ट माने या नहीं।   
.   
जहां तक हमारी बात है। हम रिकालिस्ट्स पिछले 20 सालो से निरंतर यह कह रहे है कि भारत के 80% से ज्यादा जज भ्रष्ट है, और इन्हें ठीक करने के लिए हमें राईट टू रिकॉल जज एवं जूरी सिस्टम की तत्काल जरूरत है। हमारे द्वारा प्रस्तावित कानूनों के लागू होने के महीने भर में इन स्वयंभू "मीलॉर्ड़ो" का नशा उतर जाएगा और ये जमीन पर चलने लगेंगे।   
.   
प्रस्तावित कानूनों के लिंक देखने के लिए मेरी फेसबुक प्रोफाइल के कवर पिक्चर को क्लिक करे।   
.   
और जो भी व्यक्ति / कार्यकर्ता न्यायपालिका में भ्रष्टाचार पर चिंता जाहिर करता है , उससे न्य्यापलिका में सुधार के लिए आवश्यक कानूनों के ड्राफ्ट देने को कहें। आप देखेंगे कि समाधान के लिए ड्राफ्ट मांगते ही ऐसे कथित फर्जी सुधारवादी कार्यकर्ता फरार हो जायेंगे। वे सिर्फ न्यायपालिका में भ्रष्टाचार पर चिंतन करना चाहते है , समाधान नहीं चाहते। समाधान को "टालने" का उनका यह अपना तरीका है। और यदि आप जजों के भ्रष्टाचार पर अंकुश लगाकर न्यायपालिका को जनता के प्रति जवाबदेह बनाना चाहते है तो राइट टू रिकॉल जज एवं ज्यूरी सिस्टम क़ानूनो का समर्थन, प्रचार एवं इनकी मांग करे।   
.   
\----------  
.   
मैंने अपने फेसबुक एलबम में यह लिखा हुआ है कि भारत के 80% से ज्यादा जज भ्रष्ट है और शेष 20% निकम्मे है। फेसबुक एल्बम के पोस्ट का लिंक - [https://www.facebook.com/pawan.jury/posts/1454616177989947](https://www.facebook.com/pawan.jury/posts/1454616177989947 "https://www.facebook.com/pawan.jury/posts/1454616177989947")  
.

मेरी मान्यता है कि 80% से ज्यादा जज पूरी तरह से भ्रष्ट है :  
.   
मेरा मानना है कि भारत की सुप्रीम कोर्ट के जजों में से 80% पूरी तरह से भ्रष्ट है, भाई भतीजा वादी है और उन्होंने देशी-विदेशी धनिको के साथ कुटिल गठजोड़ बना रखे है। जो उन्हें पैसा देता है वे उसके हिसाब से काम करते है। शेष 20% भी किसी काम के नही है। क्योंकि वे भारत की अदालतों में किसी भी प्रकार का सुधार लाने के लिए कोई फ़िक्र मोल लेना नहीं चाहते। वे अपनी नौकरी करते है और आँखें मूँद कर पड़े रहते है।   
.   
मेरा यह भी विश्वास है कि भारत की हाई कोर्ट जजों में से भी 80% पूरी तरह से भ्रष्ट है। जो उन्हें पैसा देता है या इन्हे पदोन्नति का चुग्गा डाल देता है , वे उसके हिसाब से फैसले दे देते है।   
.   
मैं यह भी विश्वास करता हूँ कि सुप्रीम कोर्ट एवं हाई कोर्ट की तरह ही भारत की निचली अदालतों के जजों में से भी 70% पूरी तरह से भ्रष्ट है। वे पदोन्नति के भूखे तो है ही साथ ही वे अच्छी जगह पोस्टिंग पाने के लिए भी लालायित रहते है। इस वजह से वे हाई कोर्ट / सुप्रीम कोर्ट जजों को खुश रखने के नजरिये से फैसले देते है। और जो भी उन्हें पैसा देता है उसके लिए तो खैर काम करते ही है। शेष 30% भी किसी काम के नही है। क्योंकि वे भारत की अदालतों में किसी भी प्रकार का सुधार लाने के लिए कोई लोड लेना नहीं चाहते। वे अपनी नौकरी करते है और खामोश रहते है।

मैंने अपना यह रुख ट्विटर पर भी रखा है।   
.   
a) [#iBelieve](https://www.facebook.com/hashtag/ibelieve?source=feed_text&epa=HASHTAG) , [#AboutJudges](https://www.facebook.com/hashtag/aboutjudges?source=feed_text&epa=HASHTAG) , [#Over80percentSupremeJudgesAreCorruptNepoticNexused](https://www.facebook.com/hashtag/over80percentsupremejudgesarecorruptnepoticnexused?source=feed_text&epa=HASHTAG) and rest are useless   
. .

मेरी फेसबुक एलबम पर यह पोस्ट पढ़े और अपनी फेसबुक एल्बम में भी ऐसा पोस्ट रखें जिसमे यह दर्शाया गया हो कि आपकी मान्यता में भारत के कितने प्रतिशत जज भ्रष्ट है।   
.   
========