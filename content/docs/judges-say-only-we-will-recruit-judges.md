+++
date = "2019-01-26T07:43:24+00:00"
text_area = ""
the_text_heading = ""

+++
# सुप्रीम कोर्ट का कहना है कि देश में जजो की नियुक्ति 'सिर्फ' हम ही करेंगे

भारत में सुप्रीम कोर्ट के न्यायधीशो के फैसले की कहीं पर सुनवाई नहीं है। उनकी शक्तियां असीमित है। उनका फैसला मतलब अंतिम चोट। जाहिर है सभी इस ताकत को अपने काबू में रखना चाहते है। इसी ताकत को काबू करने के लिए मोदी साहेब और सुप्रीम कोर्ट आपस में लड़ा गए है।   
.   
सुप्रीम कोर्ट का कहना है कि देश में दंड देने की शक्ति धारण करने वालो (जजो) की नियुक्ति 'सिर्फ' हम ही करेंगे। नियुक्ति की इस प्रक्रिया का नाम सुप्रीम कोर्ट ने कॉलजियम रख छोड़ा है। कॉलजियम में सुप्रीम कोर्ट के तीन न्यायधीश (प्रधान न्यायधीश समेत) बंद कमरे में बैठ कर अपने 'विवेक' से यह तय करते है कि पूरे भारत के उच्च न्यायलयों और सर्वोच्च न्यायलय में किसे न्यायधीश नियुक्त किया जाए। सामान्य अर्हताओं को छोड़कर नियुक्तियों के लिए कोई परीक्षा या ट्रेक रिकॉर्ड आदि जांचने के कोई नियत मापदंड नहीं है। यदि ये तीन लोग आपसे खुश है तो आपको इनकी अनुकम्पा से विद्वान न्यायधीश का रुतबा और दंड देने की शक्ति हासिल हो सकती है।   
.   
कोल्जियम १९९२ में लागू किया गया था। तब से ही देश के विभिन्न कार्यकर्ताओ द्वारा पारदर्शिता के अभाव के कारण इस व्यवस्था की आलोचना की जा रही थी। राइट टू रिकॉल ग्रुप पिछले एक दशक से यह मांग कर रहा है कि न्यायधीशों की नियुक्ति और बर्खास्तगी का अधिकार भारत के नागरिको के पास होना चाहिए न कि कोल्जियम के पास। मोदी साहेब को भी प्रधानमन्त्री बनने पर लगा कि कोल्जियम व्यवस्था ठीक नहीं है अत: इसे दुरुस्त करने की जरुरत है। लेकिन उन्होंने इसमें जो सुधार किया उसे कोल्जियम * कोल्जियम = कोल्जियम स्क्वायर के नाम से जाना जाता है। कैसे ?  
.   
मोदी साहेब एक बिल ले आये जिसके अनुसार न्यायधीशों की नियुक्ति का अधिकार 'राष्ट्रीय न्यायिक आयोग' के पास होगा। उन्होंने सुझाया कि राष्ट्रीय न्यायिक आयोग में 3 की जगह 6 सदस्य होंगे। तीन तो वही होंगे जो कोल्जियम में होते थे शेष 3 की नियुक्ति मोदी साहेब और सोनिया गांधी करेंगे। मतलब कोर्ट चाहती है कि तीन जज भारत की न्यायपालिका चलाएंगे जबकि मोदी साहेब चाहते है कि ऐसा 6 सदस्यों द्वारा किया जाना चाहिये। तीन जज और तीन नेता। ये 6 आदमी बंद कमरे में बैठ कर देश के न्यायधीशों की नियुक्ति का फैसला करेंगे !!!  
.   
फिर भी मोदी साहेब इस राष्ट्रीय न्यायिक आयोग को 'कोल्जियम स्क्वायर' मानने को राजी नहीं हुए। अब सुप्रीम कोर्ट ने अपनी शक्ति का प्रयोग करके राष्ट्रीय न्यायिक आयोग को असंवैधानिक घोषित कर दिया है।   
.

.   
भारत की अदालतों में तीन करोड़ केस लटके पड़े है। मतलब 3 करोड़ वादी + 3 करोड़ प्रतिवादी + 6 करोड़ गवाह + 10 करोड़ वादी प्रतिवादियों के पारिवारिक सदस्य = 22 करोड़ लोगो को तो हमारी अदालतों ने ही उलझा कर रखा है। कितने इनमे से दोषी है कितने निर्दोष, मालूम नहीं। कितने कारखाने और कितने उनके मजदूर इनमे शामिल है, अंदाजा लगाने की बात है। केस दर्ज करवाइये और फैसला आने तक टापते रहिये।   
.   
लेकिन अदालते जितनी सुस्त है उससे ज्यादा भ्रष्ट है। भाई भतीजा वाद व्यवस्था का हिस्सा है। उस पर पलेथन यह कि जजो को भ्रष्ट कहने पर जजो की अवमानना होती है, और आवाज उठाने वाले को जेल में फेंक दिया जाता है। धनिक वर्ग वर्ग और नेता आसानी से जजो घूस देकर अपने पक्ष में फैसले निकलवा लेते है। हमारे सारे क़ानून नाम मात्र के प्रतीत होते है, क्योंकि क़ानून तोड़ने पर बरसों तक फैसला नहीं आता और बड़े आदमी हमेशा जजो को 'खुश' करके बच निकलते है।   
.   
अदालते नागरिको के लिए है, लेकिन न तो नागरिको बहुमत का प्रयोग करके जजो को नौकरी से निकाल सकते है न ही उन्हें नियुक्त कर सकते है।   
.   
राईट टू रिकॉल ग्रुप मांग कर रहा है कि न्यायधीशों की नियुक्ति और बर्खास्तगी का अधिकार नागरिको के पास होना चाहिए, ताकि नागरिक बहुमत का प्रयोग करके भ्रष्ट जजो को नौकरी से निकाल सके। प्रस्तावित कानूनी ड्राफ्ट निचे दिए गए लिंक पर देखे जा सकते है। यदि आप इन कानूनी प्रक्रियाओ का समर्थन करते है तो अपने सांसद को एसएमएस भेजकर आदेश करे कि इन कानूनो को गैजेट में प्रकाशित किया जाए।

पारदर्शी शिकायत प्रणाली के लिए प्रस्तावित कानूनी ड्राफ्ट :  
[www.facebook.com/pawan.jury/posts/809753852476186](https://www.facebook.com/pawan.jury/posts/809753852476186)  
.   
ज्यूरी सिस्टम के लिए प्रस्तावित कानूनी ड्राफ्ट :  
[www.facebook.com/pawan.jury/posts/809746209143617](https://www.facebook.com/pawan.jury/posts/809746209143617)  
.  
राइट-टू-रिकॉल जिला प्रधान जज के लिए प्रस्तावित कानूनी ड्राफ्ट :  
[www.facebook.com/pawan.jury/posts/826540930797478](https://www.facebook.com/pawan.jury/posts/826540930797478)

.   
सुप्रीम कोर्ट के न्यायधीशों का कहना है कि जजो की नियुक्ति, पदोन्नति और बर्खास्तगी का अधिकार सिर्फ उनके पास होना चाहिए।   
.   
सोनिया-मोदी-केजरीवाल का कहना है कि जजो की नियुक्ति, पदोन्नति और बर्खास्तगी का अधिकार सुप्रीम कोर्ट के जजो और नेताओ के पास होना चाहिए।   
.   
सोनिया-मोदी-केजरीवाल और सुप्रीम कोर्ट के जज इस बात पर एकमत है कि जजो की नियुक्ति, पदोन्नति और बर्खास्तगी का अधिकार कम से कम नागरिको के पास तो हरगिज नही होना चाहिए।   
.   
संघ के सरसंचालक भागवत भी जजो की नियुक्ति, पदोन्नति और बर्खास्तगी का अधिकार नागरिको को दिए जाने का विरोध कर रहे है। उनका कहना है कि यदि बीजेपी सत्ता में हो तो जजो की नियुक्ति, पदोन्नति और बर्खास्तगी का अधिकार बीजेपी और सुप्रीम कोर्ट के जजो के पास और यदि कांग्रेस सत्ता में हो तो जजो की नियुक्ति, पदोन्नति और बर्खास्तगी का अधिकार सिर्फ सुप्रीम कोर्ट के जजो के पास होना चाहिए।   
.   
सभी मोदी-सोनिया-केजरीवाल-भागवत भक्त भी जजो की नियुक्ति, पदोन्नति और बर्खास्तगी का अधिकार नागरिको को दिए जाने का विरोध कर रहे है।